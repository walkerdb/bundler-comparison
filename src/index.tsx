import * as React from 'react';
import * as ReactDom from 'react-dom';

const SomeExpensiveComponent = React.lazy(() => import('./aDynamicImport')); // Lazy-loaded

ReactDom.render((
  <>
    <div>
      Hello World
    </div>
    <React.Suspense fallback={<div>Loading...</div>}>
      <SomeExpensiveComponent someProp="yep"/>
    </React.Suspense>
  </>
), document.getElementById('root'))
