# rspack

* Cold build time: ~125ms
* Warm build time (no change): ~125ms
* Warm build time (small change): ~125ms
* Dev server live-reload time: 35ms
* Built size: ~140k

## Notes

* has much nicer defaults than webpack eg built-in typescript, html templating support, and react HMR
* quite fast, feels like it would be a fine drop-in webpack replacement
* unlike other bundlers like parcel it won't take an html file as an entrypoint, instead requiring a js entry with an html template it will inject the built file link into. Not a big deal since it's easily worked around.

## Generated HTML file
Produces the below verbatim.

Also should note that unlike all other bundlers so far it expects a js file as an entry point, and injects the built entrypoint
into your html template.
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Bundle tests</title>
<script src="main.js" defer></script></head>
<body>
    <div id="root"></div>


</body></html>
```
