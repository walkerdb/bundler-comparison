# Parcel

* Cold build time: ~830ms
* Warm build time (no change): ~95ms
* Warm build time (small change): ~95ms
* Dev server live-reload time: 20-100ms
* Built size: ~145kb

## Notes

* Everything Just Works, it's incredible.
* Built size is much smaller than swc bundler
* Automatically built and included both a module and non-module script to inject into the html file, meaning browsers
  that support esmodule imports can get the module version instead.
* Dev server times are fast enough to feel instant.

## Generated HTML file

Originally without newlines or whitespace

```html
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Bundle tests</title>
    <script type="module" src="/index.8eff503f.js"></script>
    <script src="/index.a709aaf4.js" nomodule="" defer></script>
</head>
<body>
<div id="root"></div>
</body>
</html>
```
