# Webpack (vanilla)

* Cold build time: 350ms
* Warm build time: 250ms
* Dev refresh time: 15-25ms
* Build size: 138kb

## Notes

* Also quite speedy, and faster actually than the esbuild variant somehow 🤔
* Config wonkiness continues -- for this one one swc minifier support is handled through... the terser plugin??
