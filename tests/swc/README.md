# swc
* Bundle time: ~45ms
* Build size: ~140kb

## Notes
* Bundle support is listed as experimental. Config and functionality are liable to change. There is no roadmap
bundling completion.
* Bundler documentation is not great. Module field required in types but fully undocumented, and seems to be fine empty.
Many fields literally undocumented. Had to find out what to use by looking at the esbuild benchmark scripts.
* Code splitting is unsupported; dynamic imports are just concatenated into the single bundle file.
* You need to manually include the bundle file in your html via a script reference
* But hot damn is it fast. 1-2 orders of magnitude faster bundling than rollup or parcel 
