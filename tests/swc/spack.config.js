const { config } = require("@swc/core/spack");

module.exports = config({
  entry: {
    web: __dirname + "/../../src/index.tsx",
  },
  output: {
    path: __dirname + "/dist",
  },
  module: {},
  mode: "production",
  options: {
    jsc: {
      target: "es5",
      minify: {
        inlineSourcesContent: true,
        sourceMap: true,
        compress: true,
        mangle: {
          topLevel: true,
        }
      }
    },
    inlineSourcesContent: true,
    sourceMaps: true,
    minify: true,
  }
});