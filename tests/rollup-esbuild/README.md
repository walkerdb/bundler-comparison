# Rollup (esbuild)

* Cold build time: 800ms
* Warm build time: 700ms
* Bundle size: 306kb

## Notes

* Plugin documentation is def still lacking
* This is dramatically faster than vanilla rollup
* This bundle is smaller than the vanilla rollup bundle, but still way larger than other tools, including direct esbuild. Still not sure why. 

