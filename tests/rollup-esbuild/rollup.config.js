import esbuild from 'rollup-plugin-esbuild'

export default {
  input: 'src/index.tsx',
  preserveEntrySignatures: false,
  output: [
    {
      dir: 'dist',
      format: 'cjs',
      sourcemap: true,
      name: 'web',
    },
    {
      dir: 'dist-es',
      format: 'esm',
      sourcemap: true
    }
  ],
  plugins: [
    esbuild.default({
      minify: true,
      target: 'es2015',
      optimizeDeps: {
        include: ['react', 'react-dom']
      }
    }),
  ]
}
