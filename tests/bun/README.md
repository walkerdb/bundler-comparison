# bun

* Cold build time: 25ms
* Warm build time (no change): 25ms
* Warm build time (small change): 25ms
* Dev server live-reload time: N/A (no web-focused dev server)
* Built size: ~310kb

## Notes

Pros
* quite fast
* I like that there isn't any config, you just tell it what to do via CLI options.

Cons
* On the flipside that means that a lot of functionality is hidden in CLI command options, and the help docs for the bun build command aren't the best.
* No dev server (there was one for a bit during the beta but it was removed in 1.0)
* No html handling, you'll be rolling your own html file
* The build size is not yet competitive with other tooling (2x the size of the webpack build for example)

## Generated HTML file
N/A, bun can only bundle JS and does not process HTML files.
