# Webpack (vanilla)

* Cold build time: 1.3s
* Warm build time: 1.3s
* Dev refresh time: 30-40ms
* Build size: 137kb

## Notes

* Webpack's config remains its weak spot, there's just so much to set up in so many weird places to get everything working.
For example, look at all the places in webpack.config.js I had to update just to get react-refresh working.
* Dev refresh times are actually pretty good IMO
* Build size is smallest of all tested options so far.
* Plugins are required to handle automatically injecting the built files into HTML as well. I'm not sure what it would
take to have webpack also inject esmodule script versions like parcel and vite do.
* Overall not horrific. That config though...
