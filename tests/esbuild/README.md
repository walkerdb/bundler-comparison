# esbuild
* Cold build time: 15ms
* Warm build time: 15ms
* Dev reload: 15ms, manual refresh
* Build size: 139kb

## Notes
* These are by far the fastest build times of any of the tested tools. Under half the time of swc and swc is already orders of magnitude faster than every tool other than esbuild.
* Although code-splitting isn't _officially_ supported yet it's much of the way there. You can enable it by adding `--splitting`
to the command. IMO it's prod-ready right now. The main downside is that it is only usable for es-modules builds, but
most major browsers have supported them since 2017 or so. This issue tracks latest status on this feature: https://github.com/evanw/esbuild/issues/16
* I kind of dig its CLI-only interface. Not having to fuss with config files is surprisingly nice.
* I still can't get over how fast this is. You could run esbuild **220 times** in the time it takes for vanilla rollup to run _once_.
* esbuild does officially support a limited dev server via `--servedir`, but it requires manual reloading in the browser 
to see changes. It pretty literally just does a complete build on every request, which isn't unreasonable given its full
builds are faster than most other bundlers' optimized dev-server-subset-builds. It does _not_ support live reloading like 
react-refresh, though this may be hackable using a custom `esbuild.config.js` file instead of the CLI config. See more 
conversation here: https://github.com/evanw/esbuild/issues/464
