# Vite

## v2 stats

* Cold build: 720ms
* Warm build: 720ms
* Dev reload: basically instant
* Build size: 141kb

## v3 beta stats

* Cold build: 900ms
* Warm build: 900ms
* Build size: 143kb (now also includes licenses)

## With `@vitejs/plugin-legacy` for older browser support

* Cold build: 4.2s
* Warm build: 3.3s
* Build size: 143kb modern, 138kb legacy + 92kb legacy polyfill

## Notes

* When building for a library instead of an app you'll need to set up library mode https://vitejs.dev/guide/build.html#library-mode
* Dev mode updates are insanely fast
* Vite is more opinionated than any of the other options, which is expected since that's their whole thing. But it does mean
it's slightly trickier to set up. The main difference is that it expects an index.html file at root to use as its entrypoint.
This isn't dissimilar to parcel but with vite it must be at root by default.
* We'll need to add @vitejs/plugin-legacy to correctly transpile for older browsers https://github.com/vitejs/vite/tree/main/packages/plugin-legacy
* Supporting legacy browsers leads to longer build times, but like parcel it creates two separate scripts and sets up the html file to load modern js if the browser supports it, and legacy js if not.
* Overall I think I'm a fan

## Generated HTML file

```html
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <title>Bundle tests</title>
    
  <script type="module" crossorigin src="/assets/index.2e8e9ecf.js"></script>
  <script type="module">try{import.meta.url;import("_").catch(()=>1);}catch(e){}window.__vite_is_modern_browser=true;</script>
  <script type="module">!function(){if(window.__vite_is_modern_browser)return;console.warn("vite: loading legacy build because dynamic import or import.meta.url is unsupported, syntax error above should be ignored");var e=document.getElementById("vite-legacy-polyfill"),n=document.createElement("script");n.src=e.src,n.onload=function(){System.import(document.getElementById('vite-legacy-entry').getAttribute('data-src'))},document.body.appendChild(n)}();</script>
</head>
<body>
  <div id="root"></div>
  <script nomodule>!function(){var e=document,t=e.createElement("script");if(!("noModule"in t)&&"onbeforeload"in t){var n=!1;e.addEventListener("beforeload",(function(e){if(e.target===t)n=!0;else if(!e.target.hasAttribute("nomodule")||!n)return;e.preventDefault()}),!0),t.type="module",t.src=".",e.head.appendChild(t),t.remove()}}();</script>
  <script nomodule crossorigin id="vite-legacy-polyfill" src="/assets/polyfills-legacy.08e46632.js"></script>
  <script nomodule crossorigin id="vite-legacy-entry" data-src="/assets/index-legacy.3117e759.js">System.import(document.getElementById('vite-legacy-entry').getAttribute('data-src'))</script>
</body>
</html>
```
