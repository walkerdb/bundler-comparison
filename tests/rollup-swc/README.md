# Rollup (swc)

* Cold build time: 1.3s
* Warm build time: 1.3s
* Bundle size: 443kb

## Notes

* This plugin seems broken tbh. Sourcemaps don't seem to work. 
* Build times are faster but the bundle size is massive. I don't think treeshaking is actually happening.
* Would not recommend.