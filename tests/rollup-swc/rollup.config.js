import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import swc from '@rollup/plugin-swc';

export default {
  input: 'src/index.tsx',
  preserveEntrySignatures: false,
  output: [
    {
      dir: 'dist',
      format: 'cjs',
      sourcemap: true,
      name: 'web',
    },
    {
      dir: 'dist-es',
      format: 'esm',
      sourcemap: true
    }
  ],
  plugins: [
    commonjs(),
    resolve({
      extensions: ['.ts', '.tsx', '.js', '.jsx'],
    }),
    swc({
      minify: true,
      jsc: {
        "parser": {
          "syntax": "typescript",
          "tsx": true,
          "dynamicImport": true
        },
        "transform": null,
        "target": "es5",
        "loose": false,
        "externalHelpers": false,
        "keepClassNames": false,
        "minify": {
          "inlineSourcesContent": true,
          "sourceMap": true,
          "compress": true,
          "mangle": {
            "topLevel": true
          }
        }
      }
    })
  ]
}
