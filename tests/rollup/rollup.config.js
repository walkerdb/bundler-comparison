import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import typescript from '@rollup/plugin-typescript';
import terser from '@rollup/plugin-terser';

export default {
  input: 'src/index.tsx',
  preserveEntrySignatures: false,
  output: [
    {
      dir: 'dist',
      format: 'cjs',
      sourcemap: true,
      name: 'web',
    },
    {
      dir: 'dist-es',
      format: 'esm',
      sourcemap: true
    }
  ],
  plugins: [
    resolve(),
    commonjs(),
    typescript({ tsconfig: '../../tsconfig.json' }),
    terser()
  ]
}
