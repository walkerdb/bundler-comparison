# Rollup

* Cold build time: 3.3s
* Warm build time: 3.3s
* Bundle size: 432kb

## Notes

* Figuring out the right plugins was somewhat arcane and poorly documented; had to basically look up an existing app to 
see what they did.
* Adding the esm build only added an additional .2 seconds to the total build time.
* The bundle seems weirdly big, especially compared to whatever magic parcel is doing. I may be doing something wrong
but I'm finding it quite hard to figure out what the right way to do this is.
* There seems to be no between-runs caching going on
