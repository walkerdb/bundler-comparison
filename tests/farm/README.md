# Farm

* Cold build time: ~155ms
* Warm build time (no change): ~155ms
* Warm build time (small change): ~155ms
* Dev server live-reload time: 2-10ms
* Built size: ~296kb (including 126kb of just html somehow?)

## Notes

* config seems straightforward, but it is far from No Config
* live reload times are 🔥
* buttt under the same config and source files as other bundlers, it seems to be creating a broken bundle? The page crashes in dev mode. This seems likely to be user-error but I'm not seeing any obviously wrong config options.
* possibly relatedly it's inlining a huge amount of js in the html file in the bundle, far far more than other bundlers.
* it requires some unexpected other packages to run, for example the dev server can't run without react-refresh present, and the prod build fails without core-js installed.

## Generated HTML file

Far too huge to include here. It seems like it's inlining an insane
amount of JS compared to the other bundlers.
