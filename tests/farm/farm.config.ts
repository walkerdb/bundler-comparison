export default {
  compilation: {
    input: {
      index: './src/index.html'
    },
    resolve: {
      symlinks: true
    },
    output: {
      path: './dist',
      publicPath: 'public'
    },
    css: {
      prefixer: {
        targets: ['last 2 versions', 'Firefox ESR', '> 1%', 'ie >= 11']
      }
    },
    treeShaking: true,
  },
  server: {
    cors: true,
    port: 6260,
    host: 'localhost'
  },
  plugins: [
    '@farmfe/plugin-react',
  ]
};
