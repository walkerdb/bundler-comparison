import * as React from "react";

interface Props {
  someProp: string;
}
export default ({ someProp }: Props) => <div>This component is expensive: {someProp}</div>