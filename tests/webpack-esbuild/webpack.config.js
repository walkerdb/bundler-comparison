const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { EsbuildPlugin } = require('esbuild-loader')

module.exports = {
  entry: './src/index.tsx',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
  optimization: {
    minimizer: [
      new EsbuildPlugin({
        target: 'es2015',
      })
    ]
  },
  devServer: {
    hot: true,
    client: { overlay: false },
  },
  module: {
    rules: [
      {
        test: /\.[tj]sx?$/,
        loader: 'esbuild-loader',
        options: {
          loader: 'tsx',
          target: 'es2015'
        }
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
    }),
  ].filter(Boolean),
};
