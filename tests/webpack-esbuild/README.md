# Webpack (vanilla)

* Cold build time: 450ms
* Warm build time: 270ms
* Dev refresh time: 10-20ms
* Build size: 141kb

## Notes

* ~2-5x faster. Same config woes as before.
