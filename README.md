# Bundler Comparisons

A repo to compare the following across all competing JS bundlers as of July 2022:

* Cold start build times
* Warm start build times
* Dev server performance
* Final bundle size
* Developer experience

You can compare the setup for each bundler by poking around in the `tests` folder.

## Results

See the links below for more narrative on each:
* [esbuild](tests/esbuild/README.md)
* [bun](tests/bun/README.md)
* [farm](tests/farm/README.md)
* [rspack](tests/rspack/README.md)
* [swc](tests/swc/README.md)
* [vite](tests/vite/README.md)
* [parcel](tests/parcel/README.md)
* [webpack](tests/webpack/README.md)
  * [esbuild-flavored](tests/webpack-esbuild/README.md)
  * [swc-flavored](tests/webpack-swc/README.md)
* [rollup](tests/rollup/README.md)
  * [esbuild-flavored](tests/rollup-esbuild/README.md)
  * [swc-flavored](tests/rollup-swc/README.md)


| name              | prod cold | prod warm | dev rebuild | bundle size | code splits? | min supported syntax |
|-------------------|-----------|-----------|-------------|-------------|--------------|----------------------|
| bun               | 27ms      | 27ms      | N/A         | 310kb       | ✅            | es6                  |
| esbuild           | **20ms**  | **20ms**  | **20ms**    | 139kb       | ✅            | **_es6_**            |
| farm              | 155ms     | 155ms     | 2-10ms      | 296kb       | ✅            | es6                  |
| parcel            | 830ms     | 95ms      | 20-100ms    | 145kb       | ✅            | es5                  |
| rollup (vanilla)  | 3.9s      | 3.9s      | N/A         | 432kb       | ✅            | es5                  |
| rollup (esbuild)  | 650ms     | 650ms     | N/A         | 306kb       | ✅            | **_es6_**            | 
| rollup (swc)      | 1.3s      | 1.3s      | N/A         | 443kb       | ✅            | es5                  |
| rspack            | 125ms     | 125ms     | 35ms        | 140k        | ✅            | es6                  |
| swc               | 45ms      | 45ms      | N/A         | 140kb       | ⛔️            | es5                  |
| vite              | 2.4s      | 720ms     | **10-20ms** | 141kb       | ✅            | es5                  |
| webpack (vanilla) | 1.4s      | 1.4s      | < 100ms     | **137kb**   | ✅            | es5                  |
| webpack (esbuild) | 240ms     | 240ms     | **10-20ms** | 141kb       | ✅            | **_es6_**            |
| webpack (swc)     | 350ms     | 345ms     | 15-25ms     | 137kb       | ✅            | es5                  |
